import math
import matplotlib.pyplot as plt


def main():
    x_lst = []
    y_lst = []
    try:
        command = int(input("G (1) or F (2) or Y (3)?: "))
        a = float(input("Enter а: "))
        x = float(input("Enter x: "))
        xmax = float(input("Enter x maximum: "))
        number = int(input("Number of calculation steps: "))
        size = (xmax-x)/number
    except ValueError:
        print("The input is incorrect")
        return 1

    step = 0

    for i in range(number):
        if command == 1:
            try:
                g = -(8*(7*a**2+34*a*x-5*x**2)/(27*a**2+33*a*x+10*x**2))
                x += size
                step += 1
                print(step,"x=",x, "G = {0:.3f} ".format(g))
                x_lst.append(x)
                y_lst.append(g)

            except ZeroDivisionError:
                g = None
                x += size
                step += 1
                print(step, "x=", x, "G = {0:.3f} ".format(g))

        elif command == 2:
            try:
                f = -(1/(math.sin(72*a**2-5*a*x-12*x**2-(math.pi/2))))
                x += size
                step += 1
                print(step,"x=",x, "F = {0:.3f} " .format(f))
                x_lst.append(x)
                y_lst.append(f)

            except ValueError:
                f = None
                x += size
                step += 1
                print(step, "x=", x, "F = {0:.3f} " .format(f))

        elif command == 3:
            try:
                
                y = (math.log(42*a**2+53*a*x+15*x**2+1))
                x += size
                step += 1
                print(step,"x=",x, "Y = {0:.3f} " .format(y))
                x_lst.append(x)
                y_lst.append(y)

            except ValueError:
                y = None
                x += size
                step += 1
                print(step, "x=", x, "Y = {0:.3f} " .format(y))
        else:
            print("There is no such command")

    plt.xlabel('x --->')
    plt.ylabel('f(x) --->')
    plt.title('Graph of function')
    plt.plot(x_lst, y_lst, 'r.-')
    plt.show()
    fmax = max(y_lst)
    fmin = min(y_lst)
    print("f(x) minimum = {0:.3f}   f(x) maximum = {0:.3f} ".format(fmin, fmax))
while True or OverflowError:
    main()
    again = input("Do you want to start the program again? [Y/N]: ")
    if again not in ["Y", "y"]:
        break